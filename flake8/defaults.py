"""Constants that define defaults."""

EXCLUDE = '.svn,CVS,.bzr,.hg,.git,__pycache__,.tox'
IGNORE = 'E121,E123,E126,E226,E24,E704'
MAX_LINE_LENGTH = 79

# Other consants
WHITESPACE = frozenset(' \t')
