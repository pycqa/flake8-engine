.. flake8 documentation master file, created by
   sphinx-quickstart on Tue Jan 19 07:14:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Flake8: Your Tool For Style Guide Enforcement
=============================================

User Guide
----------

.. toctree::
    :maxdepth: 2

Plugin Developer Guide
----------------------

.. toctree::
    :maxdepth: 2

    dev/formatters
    dev/registering_plugins

Developer Guide
---------------

.. toctree::
    :maxdepth: 2

    internal/formatters
    internal/option_handling
    internal/plugin_handling
    internal/utils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
